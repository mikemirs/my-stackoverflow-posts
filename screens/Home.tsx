import * as React from "react";
import {
  Linking,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import { Card, TextInput, useTheme } from "react-native-paper";

import { RootTabScreenProps } from "../types";
import { useDataApi } from "../hooks/useDataApi";
import AvatarComponent from "../components/Avatar/Avatar";

type Props = {
  darkMode: boolean;
} & RootTabScreenProps<any>;

import QList from "../components/QList/QList";
import { FontAwesome } from "@expo/vector-icons";
import SubmitID from "../components/SubmitID/SubmitID";
import { useCallback } from "react";
const URL = "https://api.stackexchange.com/2.2/users/";

export default function ({ navigation: { navigate }, darkMode }: Props) {
  const [text, setText] = React.useState<string>("");
  const [state, setURL] = useDataApi("", {});
  const [sort, setSort] = React.useState<string>("creation_date");

  //1264804
  const handleIDSubmit = useCallback(() => {
    setURL(
      URL + `${text}/questions?order=desc&sort=activity&site=stackoverflow`
    );
  }, [text]);
  const handleTextChange = useCallback((t: string) => {
    setText(t);
  }, []);

  const items =
    state?.data?.items?.length > 0
      ? state?.data?.items.sort((a: any, b: any) => {
          return b[sort] - a[sort];
        })
      : undefined;
  return (
    <View
      style={[
        styles.container,
        { backgroundColor: darkMode ? "black" : "white" },
      ]}
    >
      <AvatarComponent
        darkMode={darkMode}
        owner={items ? items[0]?.owner : undefined}
        loading={state?.isLoading}
      />
      <SubmitID
        value={text}
        darkMode={darkMode}
        onSortHandle={(s) => {
          setSort(s);
        }}
        handleIDSubmit={handleIDSubmit}
        onChangeText={handleTextChange}
      />

      <QList
        questions={items}
        loading={state?.isLoading}
        darkMode={darkMode}
        onCardPress={(url) => {
          if (Platform.OS === "web") {
            window.open(url, "_blank");
            return;
          }
          navigate("Modal", { url });
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 20,

    fontWeight: "bold",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
});
