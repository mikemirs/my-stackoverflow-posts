import { Feather, FontAwesome, MaterialIcons } from "@expo/vector-icons";
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from "@react-navigation/native";

import { createNativeStackNavigator } from "@react-navigation/native-stack";
import * as React from "react";
import { ColorSchemeName, Pressable } from "react-native";

import ModalScreen from "../screens/ModalScreen";
import NotFoundScreen from "../screens/NotFoundScreen";
import Home from "../screens/Home";

import { RootStackParamList, RootTabScreenProps } from "../types";
import LinkingConfiguration from "./LinkingConfiguration";
import { Appbar, Switch, useTheme } from "react-native-paper";
import { View } from "react-native";

export default function Navigation({
  colorScheme,
}: {
  colorScheme: ColorSchemeName;
}) {
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
    >
      <RootNavigator />
    </NavigationContainer>
  );
}

/**
 * A root stack navigator is often used for displaying modals on top of all other content.
 * https://reactnavigation.org/docs/modal
 */
type DarkModeProps = {
  isSwitchOn: boolean;
  onToggleSwitch: () => void;
};

const Stack = createNativeStackNavigator<RootStackParamList>();
const DarkMode = ({ isSwitchOn, onToggleSwitch }: DarkModeProps) => {
  return (
    <View style={{ flexDirection: "row", alignItems: "center" }}>
      <Switch value={isSwitchOn} onValueChange={onToggleSwitch} />
      {isSwitchOn ? (
        <MaterialIcons name="nightlight-round" size={21} color="white" />
      ) : (
        <Feather name="sun" size={21} color="white" />
      )}
    </View>
  );
};
function RootNavigator() {
  const [isSwitchOn, setIsSwitchOn] = React.useState(false);
  const { colors } = useTheme();
  const onToggleSwitch = React.useCallback(
    () => setIsSwitchOn(!isSwitchOn),
    [isSwitchOn]
  );
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: isSwitchOn ? colors.dark : colors.primary,
        },
        headerTintColor: isSwitchOn ? colors.primary : "#FFF",
        headerTitleStyle: {
          fontWeight: "bold",
        },
        headerRight: () => (
          <DarkMode onToggleSwitch={onToggleSwitch} isSwitchOn={isSwitchOn} />
        ),
      }}
    >
      <Stack.Screen
        name="Root"
        options={({ navigation }: RootTabScreenProps<"Root">) => ({
          title: "Stackoverflow questions",
        })}
      >
        {(props) => <Home darkMode={isSwitchOn} {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name="NotFound"
        component={NotFoundScreen}
        options={{ title: "Oops!" }}
      />
      <Stack.Group screenOptions={{ presentation: "modal" }}>
        <Stack.Screen name="Modal" component={ModalScreen} />
      </Stack.Group>
    </Stack.Navigator>
  );
}

function TabBarIcon(props: {
  name: React.ComponentProps<typeof FontAwesome>["name"];
  color: string;
}) {
  return <FontAwesome size={30} style={{ marginBottom: -3 }} {...props} />;
}
