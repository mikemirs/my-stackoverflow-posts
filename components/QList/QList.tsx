import { SafeAreaView, ScrollView, StyleSheet, Text, View } from "react-native";
import * as React from "react";
import {
  ActivityIndicator,
  Badge,
  Card,
  Paragraph,
  Title,
  useTheme,
} from "react-native-paper";
// @ts-ignore
import _ from "lodash";
import { useEffect, useRef } from "react";
type Props = {
  loading: boolean;
  darkMode: boolean;
  onCardPress: (url: string) => void;
  questions: {
    answer_count: number;
    view_count: number;
    tags: Array<string>;
    score: number;
    link: string;
    creation_date: number;
    title: string;
  }[];
};
const QList = ({ loading, questions, darkMode, onCardPress }: Props) => {
  const { colors } = useTheme();

  return (
    <View style={styles.container}>
      {loading ? (
        <View
          style={{
            flex: 1,
            minHeight: 100,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <ActivityIndicator size={"large"} />
        </View>
      ) : (
        <ScrollView style={{ margin: 5 }} onScrollEndDrag={() => {}}>
          {questions?.map((q) => {
            return (
              <Card
                onPress={() => onCardPress(q?.link)}
                key={_.uniqueId()}
                style={{
                  margin: 10,
                  backgroundColor: darkMode ? colors.dark : "#fdf7f1",
                }}
              >
                <Card.Content
                  style={{
                    flexDirection: "row",
                  }}
                >
                  <View style={{ flex: 1 }}>
                    <Text style={{ alignSelf: "flex-end" }}>
                      {new Date(q.creation_date * 1000).toDateString()}
                    </Text>
                    <Title style={{ color: darkMode ? "white" : "black" }}>
                      {q?.title}
                    </Title>
                    <Paragraph
                      style={{ color: darkMode ? "white" : "black" }}
                    >{`Answers: ${q.answer_count}`}</Paragraph>
                    <Paragraph
                      style={{ color: darkMode ? "white" : "black" }}
                    >{`Views: ${q.view_count}`}</Paragraph>

                    <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                      {q?.tags?.map((t: string) => {
                        return (
                          <View style={[styles.tag]}>
                            <Text style={{ color: "#fff" }}>{"⏺ " + t}</Text>
                          </View>
                        );
                      })}
                    </View>
                  </View>
                </Card.Content>
              </Card>
            );
          })}
        </ScrollView>
      )}

      {questions?.length && (
        <Text
          style={{
            fontSize: 15,
            margin: 15,
            color: darkMode ? colors.primary : "#000",
          }}
        >{`Total questions: ${questions.length}`}</Text>
      )}
    </View>
  );
};
export default QList;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tag: {
    padding: 4,
    borderRadius: 5,
    backgroundColor: "#343434",
    flexDirection: "row",
    alignItems: "center",
    margin: 2,
  },
});
