import {
  Keyboard,
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import * as React from "react";
import {
  Button,
  Card,
  IconButton,
  TextInput,
  ToggleButton,
  useTheme,
} from "react-native-paper";
import {
  EvilIcons,
  FontAwesome,
  Fontisto,
  MaterialIcons,
} from "@expo/vector-icons";
type Props = {
  value: string;
  darkMode: boolean;
  handleIDSubmit: () => void;
  onChangeText: (s: string) => void;
  onSortHandle: (t: string) => void;
};
const SortButton = ({
  children,
  title,
  color,
  onPress,
}: {
  title: string;
  color: string;
  onPress: () => void;
  children: JSX.Element;
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        alignItems: "center",
        flexDirection: "row",
        borderBottomWidth: 1,
        padding: 2,
        borderColor: color,
        borderRadius: 2,
      }}
    >
      {children}
      <Text style={{ fontWeight: "bold", fontSize: 12, color: color }}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};
const SubmitID = ({
  value,
  darkMode,
  handleIDSubmit,
  onChangeText,
  onSortHandle,
}: Props) => {
  const { colors } = useTheme();

  return (
    <View style={styles.container}>
      <View style={styles.input}>
        <View style={{ flex: 1 }}>
          <TextInput
            label="User ID"
            mode="flat"
            keyboardType={"numeric"}
            placeholder={"Please submit user id."}
            value={value}
            dense
            theme={{
              dark: darkMode,
              colors: { placeholder: colors.primary },
            }}
            style={[darkMode ? { backgroundColor: colors.dark } : {}]}
            onSubmitEditing={handleIDSubmit}
            onChangeText={onChangeText}
          />
        </View>
        {value.length > 0 && (
          <TouchableOpacity
            style={{ margin: 5 }}
            onPress={() => {
              Keyboard.dismiss();
              handleIDSubmit();
            }}
          >
            <FontAwesome name="search" size={24} color="black" />
          </TouchableOpacity>
        )}
      </View>
      <Card
        style={{
          marginLeft: 15,
          marginRight: 15,
          backgroundColor: darkMode ? colors.dark : "#fff",
        }}
      >
        <Card.Content>
          <Text
            style={{
              fontWeight: "bold",
              padding: 5,
              fontSize: 10,
              color: darkMode ? colors.primary : "#000",
            }}
          >
            Sort by
          </Text>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <SortButton
              title={"Date"}
              color={colors.primary}
              onPress={() => onSortHandle("creation_date")}
            >
              <Fontisto
                name="date"
                size={24}
                color={colors.primary}
                style={{ marginRight: 10 }}
              />
            </SortButton>
            <SortButton
              title={"Views"}
              color={colors.primary}
              onPress={() => onSortHandle("view_count")}
            >
              <MaterialIcons
                color={colors.primary}
                name="preview"
                size={25}
                style={{ marginRight: 10 }}
              />
            </SortButton>
            <SortButton
              title={"Answers"}
              color={colors.primary}
              onPress={() => onSortHandle("answer_count")}
            >
              <EvilIcons
                name="comment"
                size={32}
                color={colors.primary}
                style={{ marginRight: 10 }}
              />
            </SortButton>
          </View>
        </Card.Content>
      </Card>
    </View>
  );
};
export default SubmitID;
const styles = StyleSheet.create({
  container: {},
  input: { flexDirection: "row", alignItems: "center", padding: 15 },
});
