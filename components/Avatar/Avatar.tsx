import { StyleSheet, View, Text, Image } from "react-native";
import { Card, Avatar, ActivityIndicator, useTheme } from "react-native-paper";
import * as React from "react";
const defaultAvatar = require("../../assets/images/avatar.png");
type Props = {
  owner:
    | {
        display_name: string;
        reputation: string;
        profile_image: string;
      }
    | undefined;
  loading: boolean;
  darkMode?: boolean;
};
const AvatarComponent = ({ owner, loading, darkMode }: Props) => {
  const { colors } = useTheme();
  const textColorWhenDark = darkMode
    ? { color: colors.primary }
    : { color: colors.dark };

  return (
    <View style={styles.container}>
      {loading ? (
        <View
          style={{
            flex: 1,
            minHeight: 100,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <ActivityIndicator size={"large"} />
        </View>
      ) : (
        <>
          <Avatar.Image
            size={100}
            source={
              owner?.profile_image
                ? { uri: owner?.profile_image }
                : defaultAvatar
            }
          />
          <View style={{ margin: 16 }}>
            <Text style={[styles.title, textColorWhenDark]}>
              {owner?.display_name || ""}
            </Text>
            <Text style={[styles.subtitle, textColorWhenDark]}>
              {owner?.reputation ? `Reputation ${owner?.reputation}` : ""}
            </Text>
          </View>
        </>
      )}
    </View>
  );
};
export default AvatarComponent;
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flexDirection: "row",
    margin: 12,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
  },
  subtitle: {},
});
